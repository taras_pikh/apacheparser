package com.parser.service;

import com.parser.utils.Utils;
import org.apache.hc.client5.http.ClientProtocolException;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.HttpClientResponseHandler;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserService {

    public static int getParsedCurrentDateCount() {
        List<String> contentList = getContentList();
        int dateCount = 0;
        for (String content : contentList) {
            if (ifTodayDay(content)) {
                dateCount++;
            }
        }
        return dateCount;
    }

    private static String getSiteContentData(String url) {
        try (final CloseableHttpClient httpclient = HttpClients.createDefault()) {
            final HttpGet httpGet = getGetRequest(url);
            // Create a custom response handler
            final HttpClientResponseHandler<String> responseHandler = getResponseHandler();
            return httpclient.execute(httpGet, responseHandler);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    private static HttpClientResponseHandler getResponseHandler() {
        return new HttpClientResponseHandler<String>() {
            @Override
            public String handleResponse(final ClassicHttpResponse response) throws IOException {
                final int status = response.getCode();
                if (status >= HttpStatus.SC_SUCCESS && status < HttpStatus.SC_REDIRECTION) {
                    final HttpEntity entity = response.getEntity();
                    try {
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } catch (final ParseException ex) {
                        throw new ClientProtocolException(ex);
                    }
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };
    }

    private static HttpGet getGetRequest(String url) {
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        return httpGet;
    }

    private static List<String> getLinks() {
        ArrayList<String> links = new ArrayList<String>();
        links.add("https://www.opennet.ru/opennews/mini.shtml");
        links.add("https://mirknig.su");
        return links;
    }

    private static List<String> getContentList() {
        List<String> contentList = new ArrayList<String>();
        for (String url : getLinks()) {
            String siteContentData = getSiteContentData(url);
            contentList.addAll(Arrays.asList(siteContentData.split("\\n")));
        }
        return contentList;
    }

    private static boolean ifTodayDay(String content) {
        Pattern p = Pattern.compile(".*([01]?[0-9]|2[0-3]):[0-5][0-9].*");
        Matcher m = p.matcher(content);
        return content.contains("<td class=tdate>" + Utils.parseDate(new Date()) + "</td>")  || (content.contains("Сегодня, ") && m.matches());
    }
}
