package com.parser.utils;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static String parseDate(Date date) {
        Format formatter = new SimpleDateFormat("dd.MM.yyyy");
        return formatter.format(date);
    }
}
